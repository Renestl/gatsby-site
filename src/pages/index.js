import React from 'react'
import Link from 'gatsby-link'

const IndexPage = () => (
  <div>
    <h1>Hi everyone!</h1>
    <p>I'm Jennifer Currie and I'm quite happy to make your acquaintance. </p>
    <Link to="/page-2/">Go to page 2</Link>
  </div>
)

export default IndexPage
